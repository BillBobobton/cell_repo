﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    public GameController gc { get; protected set; }

    public bool divide { get; protected set; }
    public Button divideButton;
    
    void Start ()
    {
        gc = FindObjectOfType<GameController>();
        divide = false;
    }

    public void DivideCell()
    {
        if (divide == true && gc.selectedCell != null)
        {
            gc.oc.SpawnCell(
                gc.selectedCell.cellType,
                gc.selectedCell.posX + 1,
                gc.selectedCell.posY + 1
                );
        }
    }

    public void ToggleDivideMode ()
    {
        if (divide == false)
        {
            divide = true;
        }
        else
        {
            divide = false;
        }
    }
}
