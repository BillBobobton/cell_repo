﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour {
    
    public enum CellType {stem, absorb, attack, defend};
    public CellType cellType { get; protected set; }
    public int posX { get; protected set; }
    public int posY { get; protected set; }
    public int numNeighbours { get; protected set; }

    public bool selected = false;
    SpriteRenderer sr;

    float deltaTime = 0f;

    OrganismController oc;

    public void InitialiseCell (CellType type, int x, int y)
    {
        this.cellType = type;
        this.posX = x;
        this.posY = y;
    }

    private void Start()
    {
        // get the game controller
        oc = FindObjectOfType<OrganismController>();
        sr = GetComponentInChildren<SpriteRenderer>();

        // start with 0 neighbours
        numNeighbours = 0;

        // set the initial cell colour
        SetCellColour();
    }

    private void Update()
    {
        if (deltaTime > 2f)
        {
            Absorb();
            deltaTime = 0f;
            return;
        }

        deltaTime += Time.deltaTime;
    }

    public void SelectCell()
    {
        // ADD ERROR HANDLING
        if (selected == false)
        {
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, 0.5f);
            selected = true;
        }
        else
            UnselectCell();
    }

    public void UnselectCell ()
    {
        // ADD ERROR HANDLING
        {
            SetCellColour();
            selected = false;
        }
    }

    public void SetCellColour()
    {
        if (cellType == CellType.stem)
            sr.color = Color.white;
        if (cellType == CellType.absorb)
            sr.color = Color.green;
        if (cellType == CellType.attack)
            sr.color = Color.red;
        if (cellType == CellType.defend)
            sr.color = Color.blue;
    }

    void Absorb()
    {
        if (numNeighbours < 0 || numNeighbours > 4)
        {
            Debug.LogError("Cell::Absorb:: - Invalid numNeighbours - make sure value was " +
                           "set before adding to cells list in OrganismController");
            return;
        }

        int openEdges = 4 - numNeighbours;

        if (cellType == CellType.stem)
            oc.gc.AddNutrient(1 * openEdges);
        if (cellType == CellType.absorb)
            oc.gc.AddNutrient(5 * openEdges);
        if (cellType == CellType.attack)
            oc.gc.AddNutrient(-1);
        if (cellType == CellType.defend)
            oc.gc.AddNutrient(-1);
    }

    public void AddNeighbour(int n)
    {
        numNeighbours += n;
    }
}
