﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrganismController : MonoBehaviour {

    public Cell cellPrefab; //{ get; protected set; }
    public List<Cell> cells { get; protected set; }

    public GameController gc { get; protected set; }

    // Use this for initialization
    void Start () {
        gc = FindObjectOfType<GameController>();

        // initialise list of cells
        cells = new List<Cell>();

        SpawnCell(Cell.CellType.stem, 0, 0);
    }
	
    public void SpawnCell (Cell.CellType cellType, int x, int y)
    {
        if (FindCellAt(x,y) != null)
        {
            Debug.Log("Trying to spawn cell where one already exists");
            return;
        }

        // Instantiate initial cell
        Cell newCell = Instantiate<Cell>(cellPrefab);
        // Initialise cell
        newCell.InitialiseCell(cellType, x, y);
        // Set position based upon initial values
        newCell.transform.position = new Vector2(newCell.posX, newCell.posY);

        foreach (Cell c in cells)
        {
            Debug.Log(Mathf.Abs(c.posX - newCell.posX) + Mathf.Abs(c.posY - newCell.posY));

            if (Mathf.Abs(c.posX - newCell.posX) + Mathf.Abs(c.posY - newCell.posY) == 1)
            {
                newCell.AddNeighbour(1);
                c.AddNeighbour(1);
            }
        }

        // Add newCell to cells list
        cells.Add(newCell);
    }

    public void PlaceCell ()
    {

    }

    public Cell FindCellAt (int x, int y)
    {
        foreach (Cell c in cells)
        {
            if (x == c.posX && y == c.posY)
            {
                return c;
            }
        }

        Debug.Log("OrganismController::FindCellAt:: - No cell found at:" + x + "," + y);
        return null;
    }
}
