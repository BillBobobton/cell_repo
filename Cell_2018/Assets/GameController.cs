﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public Text nutrientText;
    int nutrient = 0;
    public OrganismController oc { get; protected set; }
    public Cell selectedCell { get; protected set; }

    private void Start()
    {
        oc = FindObjectOfType<OrganismController>();
        selectedCell = null;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            oc.SpawnCell(Cell.CellType.attack, 0, 1);
            oc.SpawnCell(Cell.CellType.attack, 0, -1);
            oc.SpawnCell(Cell.CellType.attack, 1, 0);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            oc.SpawnCell(Cell.CellType.stem, 1, 1);
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (selectedCell == null)
                SelectCell();
            else
            {
                Vector2 clickPos = GetClick();
                if (clickPos != new Vector2(selectedCell.posX, selectedCell.posY))
                {
                    Debug.Log(Mathf.Abs(selectedCell.posX - clickPos.x));
                    if (Mathf.Abs(selectedCell.posX - clickPos.x) > 0 && clickPos.y == 0)
                    {
                        oc.SpawnCell(selectedCell.cellType, selectedCell.posX + 1, selectedCell.posY);
                        return;
                    }
                    if (Mathf.Abs(selectedCell.posX - clickPos.x) < 0 && clickPos.y == 0)
                    {
                        oc.SpawnCell(selectedCell.cellType, selectedCell.posX - 1, selectedCell.posY);
                        return;
                    }
                    if (Mathf.Abs(selectedCell.posY - clickPos.y) > 0 && clickPos.x == 0)
                    {
                        oc.SpawnCell(selectedCell.cellType, selectedCell.posX, selectedCell.posY + 1);
                        return;
                    }
                    if (Mathf.Abs(selectedCell.posY - clickPos.y) < 0 && clickPos.x == 0)
                    {
                        oc.SpawnCell(selectedCell.cellType, selectedCell.posX, selectedCell.posY - 1);
                        return;
                    }
                    else
                        return;
                }
                selectedCell.UnselectCell();
                selectedCell = null;
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            if (selectedCell == null)
                SelectCell();
            else
            {
                selectedCell.UnselectCell();
                selectedCell = null;
            }
        }
    }

    void SelectCell ()
    {
        Vector2 selectLoc = GetClick();
        foreach (Cell c in oc.cells)
        {
            if (selectLoc.x == c.posX && selectLoc.y == c.posY)
            {
                c.SelectCell();
                selectedCell = c;
            }
        }
    }

    public void AddNutrient(int amount)
    {
        nutrient += amount;
        nutrientText.text = "Nutrient: " + nutrient.ToString();
    }

    public Vector2 GetClick()
    {
        Vector2 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        clickPos.x = Mathf.Floor(clickPos.x);
        clickPos.y = Mathf.Floor(clickPos.y);

        return clickPos;
    }
}
